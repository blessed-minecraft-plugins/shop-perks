package fr.blessedraccoon.minecraft.plugins.shopperks.shop;

import fr.blessedraccoon.minecraft.plugins.shopperks.games.ballsofsteel.*;

import java.util.List;

public class Perks {

    public final static List<ItemPerk> ballsOfSteelItemPerks = List.of(
            new PickaxeEfficiency(0),
            new PickaxeUnbreaking(1),
            new PickaxeFortune(2),
            new PickaxeBetter(3),
            new SwordBetter(4)
    );

}
