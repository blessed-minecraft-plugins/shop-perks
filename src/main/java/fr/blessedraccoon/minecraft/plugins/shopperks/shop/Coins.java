package fr.blessedraccoon.minecraft.plugins.shopperks.shop;

import de.NeonnBukkit.CoinsAPI.API.CoinsAPI;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class Coins {

    /**
     * Pays a transaction if player has enough coins to afford
     * @param player player
     * @param amount amount of coins to pay
     * @return true if the transaction succeeded (if the player has enough coins to afford)
     */
    public static boolean pay(@NotNull Player player, int amount) {
        int coins = CoinsAPI.getCoins(player.getUniqueId().toString());
        if (coins - amount >= 0) {
            CoinsAPI.removeCoins(player.getUniqueId().toString(), amount);
            return true;
        }
        return false;
    }

    public static boolean canAfford(@NotNull Player player, int amount) {
        int coins = CoinsAPI.getCoins(player.getUniqueId().toString());
        return coins - amount >= 0;
    }

}
