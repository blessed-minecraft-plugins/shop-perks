package fr.blessedraccoon.minecraft.plugins.shopperks.shop;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public enum GenericMaterial {

    SWORDS("SWORD"),
    PICKAXES("PICKAXE"),
    BOWS("BOW");

    private String research;

    GenericMaterial(String str) {
        this.research = str;
    }

    public List<ItemStack> find(PlayerInventory inv) {
        return Arrays.stream(inv.getContents()).filter(Objects::nonNull).filter(item -> item.getType().name().contains(this.research)).toList();
    }

    public static List<ItemStack> find(PlayerInventory inv, ItemStack ...items) {
        return Arrays.stream(inv.getContents()).filter(Objects::nonNull).filter(item -> {
            for(var it : items) {
                if (item.getType() == it.getType()) {
                    return true;
                }
            }
            return false;
        }).toList();
    }

}
