package fr.blessedraccoon.minecraft.plugins.shopperks.shop;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public abstract class ItemPerk extends Perk {
    public ItemPerk(int ID, ItemStack item, List<LevelCostDescription> perksLevels) {
        super(ID, item, perksLevels);
        this.setItem(this.buildItem());
        this.setPerksLevels(this.buildLevelCostDescriptions());
    }

    public ItemPerk(int id) {
        super(id);
    }

    public abstract void apply(@NotNull PlayerInventory inventory, int level);
}
