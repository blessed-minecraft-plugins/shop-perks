package fr.blessedraccoon.minecraft.plugins.shopperks.games.ballsofsteel;

import fr.blessedraccoon.minecraft.plugins.shopperks.shop.GenericMaterial;
import fr.blessedraccoon.minecraft.plugins.shopperks.shop.ItemPerk;
import fr.blessedraccoon.minecraft.plugins.shopperks.shop.LevelCostDescription;
import fr.blessedraccoon.minecraft.plugins.shopperks.shop.Perk;
import net.kyori.adventure.text.Component;
import net.md_5.bungee.api.chat.ItemTag;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.Arrays;
import java.util.List;

public class PickaxeEfficiency extends ItemPerk {

    public PickaxeEfficiency(int ID) {
        super(ID);
    }

    @Override
    public ItemStack buildItem() {
        ItemStack item = new ItemStack(Material.DIAMOND_PICKAXE);
        var meta = item.getItemMeta();
        meta.displayName(Component.text("Enchantement sur la pioche : Efficiency"));
        item.setItemMeta(meta);
        return item;
    }

    @Override
    public List<LevelCostDescription> buildLevelCostDescriptions() {
        var perksLevels = List.of(
                new LevelCostDescription(1, 1_000, "Efficiency I", "Donne l'enchantement Efficiency I sur la pioche"),
                new LevelCostDescription(2, 4_000, "Efficiency II", "Donne l'enchantement Efficiency II sur la pioche"),
                new LevelCostDescription(3, 15_000, "Efficiency III", "Donne l'enchantement Efficiency III sur la pioche"),
                new LevelCostDescription(4, 50_000, "Efficiency IV", "Donne l'enchantement Efficiency IV sur la pioche"),
                new LevelCostDescription(5, 250_000, "Efficiency V", "Donne l'enchantement Efficiency V sur la pioche"),
                new LevelCostDescription(6, 1_000_000, "Efficiency VI", "Donne l'enchantement Efficiency VI sur la pioche"),
                new LevelCostDescription(7, 3_000_000, "Efficiency VII", "Donne l'enchantement Efficiency VII sur la pioche"),
                new LevelCostDescription(8, 10_000_000, "Efficiency VIII", "Donne l'enchantement Efficiency VIII sur la pioche")
        );
        return perksLevels;
    }

    @Override
    public void apply(PlayerInventory inventory, int level) {
        var items = GenericMaterial.PICKAXES.find(inventory);
        for(var item : items) {
            var meta = item.getItemMeta();
            meta.addEnchant(Enchantment.DIG_SPEED, level, true);
            item.setItemMeta(meta);
        }
    }
}
