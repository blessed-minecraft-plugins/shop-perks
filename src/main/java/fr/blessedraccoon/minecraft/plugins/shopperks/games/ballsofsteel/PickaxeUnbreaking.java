package fr.blessedraccoon.minecraft.plugins.shopperks.games.ballsofsteel;

import fr.blessedraccoon.minecraft.plugins.shopperks.shop.GenericMaterial;
import fr.blessedraccoon.minecraft.plugins.shopperks.shop.ItemPerk;
import fr.blessedraccoon.minecraft.plugins.shopperks.shop.LevelCostDescription;
import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.List;

public class PickaxeUnbreaking extends ItemPerk {

    public PickaxeUnbreaking(int ID) {
        super(ID);
    }

    @Override
    public ItemStack buildItem() {
        ItemStack item = new ItemStack(Material.DIAMOND_PICKAXE);
        var meta = item.getItemMeta();
        meta.displayName(Component.text("Enchantement sur la pioche : Unbreaking"));
        item.setItemMeta(meta);
        return item;
    }

    @Override
    public List<LevelCostDescription> buildLevelCostDescriptions() {
        var perksLevels = List.of(
                new LevelCostDescription(1, 300, "Unbreaking I", "Donne l'enchantement Unbreaking I sur la pioche"),
                new LevelCostDescription(2, 1_000, "Unbreaking II", "Donne l'enchantement Unbreaking II sur la pioche"),
                new LevelCostDescription(3, 2_500, "Unbreaking III", "Donne l'enchantement Unbreaking III sur la pioche"),
                new LevelCostDescription(4, 7_000, "Unbreaking IV", "Donne l'enchantement Unbreaking IV sur la pioche"),
                new LevelCostDescription(5, 20_000, "Unbreaking V", "Donne l'enchantement Unbreaking V sur la pioche"),
                new LevelCostDescription(6, 50_000, "Unbreaking VI", "Donne l'enchantement Unbreaking VI sur la pioche"),
                new LevelCostDescription(7, 125_000, "Unbreaking VII", "Donne l'enchantement Unbreaking VII sur la pioche"),
                new LevelCostDescription(8, 400_000, "Incassable", "La pioche est littéralement incassable")
        );
        return perksLevels;
    }

    @Override
    public void apply(PlayerInventory inventory, int level) {
        var items = GenericMaterial.PICKAXES.find(inventory);
        for(var item : items) {
            var meta = item.getItemMeta();
            meta.addEnchant(Enchantment.DURABILITY, level, true);
            if (level == 8) {
                meta.setUnbreakable(true);
            }
            item.setItemMeta(meta);
        }
    }
}
