package fr.blessedraccoon.minecraft.plugins.shopperks.games.ballsofsteel;

import fr.blessedraccoon.minecraft.plugins.shopperks.shop.GenericMaterial;
import fr.blessedraccoon.minecraft.plugins.shopperks.shop.ItemPerk;
import fr.blessedraccoon.minecraft.plugins.shopperks.shop.LevelCostDescription;
import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.List;

public class SwordBetter extends ItemPerk {

    public SwordBetter(int ID) {
        super(ID);
    }

    @Override
    public ItemStack buildItem() {
        ItemStack item = new ItemStack(Material.IRON_SWORD);
        var meta = item.getItemMeta();
        meta.displayName(Component.text("Une meilleure épée"));
        item.setItemMeta(meta);
        return item;
    }

    @Override
    public List<LevelCostDescription> buildLevelCostDescriptions() {
        var perksLevels = List.of(
                new LevelCostDescription(1, 1_000, "Epée en fer", "Donne une épée en fer au lieu d'une épée en pierre"),
                new LevelCostDescription(2, 4_000, "Epée en diamant", "Donne une épée en diamant au lieu d'une épée en fer"),
                new LevelCostDescription(3, 15_000, "Epée en netherite", "Donne une épée en netherite au lieu d'une épée en diamant")
        );
        return perksLevels;
    }

    @Override
    public void apply(PlayerInventory inventory, int level) {
        var items = GenericMaterial.SWORDS.find(inventory);
        for(var item : items) {
            switch (level) {
                case 1:
                    item.setType(Material.IRON_SWORD);
                    break;
                case 2:
                    item.setType(Material.DIAMOND_SWORD);
                    break;
                case 3:
                    item.setType(Material.NETHERITE_SWORD);
                    break;
            }
        }
    }

}
