package fr.blessedraccoon.minecraft.plugins.shopperks.shop;

import net.kyori.adventure.text.Component;
import org.bukkit.ChatColor;

public class LevelCostDescription {

    private int level;
    private int cost;
    private Component name;

    public int getCost() {
        return cost;
    }

    public Component getName() {
        return name;
    }

    public Component getDescription() {
        return description;
    }

    public int getLevel() {
        return level;
    }

    private Component description;

    public LevelCostDescription(int level, int cost, String name, String description) {
        this.level = level;
        this.cost = cost;
        this.name = Component.text(ChatColor.DARK_AQUA.toString() + name);
        this.description = Component.text(ChatColor.DARK_AQUA.toString() + description);
    }
}
