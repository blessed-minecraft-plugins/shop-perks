package fr.blessedraccoon.minecraft.plugins.shopperks.shop;

import me.vagdedes.mysql.database.SQL;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Consumer;

import java.util.List;

public abstract class Perk implements Consumer<InventoryClickEvent> {

    private final int perkID;
    private List<LevelCostDescription> perksLevels;
    private ItemStack item;

    public Perk(int ID, ItemStack item, List<LevelCostDescription> perksLevels) {
        this.perkID = ID;
        this.item = item;
        this.perksLevels = perksLevels;
    }

    public Perk(int ID) {
        this.perkID = ID;
    }

    public void setItem(ItemStack item) {
        this.item = item;
    }

    public void setPerksLevels(List<LevelCostDescription> perksLevels) {
        this.perksLevels = perksLevels;
    }

    public abstract ItemStack buildItem();
    public abstract List<LevelCostDescription> buildLevelCostDescriptions();

    public ItemStack getItem() {
        return item;
    }

    public List<LevelCostDescription> getPerksLevels() {
        return perksLevels;
    }

    public int getPerkID() {
        return perkID;
    }

    @Override
    public void accept(InventoryClickEvent e) {
        int level = e.getSlot() % 9;
        LevelCostDescription lcd = this.getPerksLevels().stream().filter(l -> l.getLevel() == level)
                .findAny()
                .orElse(null);

        var player = (Player) e.getWhoClicked();
        var ok = Coins.pay(player, lcd.getCost());
        if (ok) {
            var uuid = player.getUniqueId().toString();
            Object res = SQL.get("PerkID", new String[]{"UUID = '" + uuid + "'", "perkID = " + this.getPerkID()}, "BallsOfSteelPerks");
            if (res == null) { // The user hasn't the perk yet
                SQL.insertData("UUID, PerkID, level",
                        String.format("'%s', %d, %d", uuid, this.getPerkID(), level), "BallsOfSteelPerks");
            } else {
                SQL.set("level", level,
                        new String[]{"uuid='" + uuid + "'", "perkID=" + this.getPerkID()}, "BallsOfSteelPerks"); // Example
            }
            e.getWhoClicked().sendMessage(ChatColor.GREEN + "Ton achat s'est déroulé correctement");
            e.getInventory().close();
        }
        else {
            e.getWhoClicked().sendMessage(ChatColor.RED + "Tu ne peux pas acheter ceci.");
        }

    }

}
