package fr.blessedraccoon.minecraft.plugins.shopperks.games.ballsofsteel;

import fr.blessedraccoon.minecraft.plugins.shopperks.shop.GenericMaterial;
import fr.blessedraccoon.minecraft.plugins.shopperks.shop.ItemPerk;
import fr.blessedraccoon.minecraft.plugins.shopperks.shop.LevelCostDescription;
import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.List;

public class PickaxeFortune extends ItemPerk {

    public PickaxeFortune(int id) {
        super(id);
    }

    @Override
    public ItemStack buildItem() {
        ItemStack item = new ItemStack(Material.DIAMOND_PICKAXE);
        var meta = item.getItemMeta();
        meta.displayName(Component.text("Enchantement sur la pioche : Fortune"));
        item.setItemMeta(meta);
        return item;
    }

    @Override
    public List<LevelCostDescription> buildLevelCostDescriptions() {
        var perksLevels = List.of(
                new LevelCostDescription(1, 7_500, "Fortune I", "Donne l'enchantement Fortune I sur la pioche"),
                new LevelCostDescription(2, 40_000, "Fortune II", "Donne l'enchantement Fortune II sur la pioche"),
                new LevelCostDescription(3, 250_000, "Fortune III", "Donne l'enchantement Fortune III sur la pioche"),
                new LevelCostDescription(4, 1_500_000, "Fortune IV", "Donne l'enchantement Fortune IV sur la pioche"),
                new LevelCostDescription(5, 10_000_000, "Fortune V", "Donne l'enchantement Fortune V sur la pioche")
        );
        return perksLevels;
    }

    @Override
    public void apply(PlayerInventory inventory, int level) {
        var items = GenericMaterial.PICKAXES.find(inventory);
        for(var item : items) {
            var meta = item.getItemMeta();
            meta.addEnchant(Enchantment.LOOT_BONUS_BLOCKS, level, true);
            item.setItemMeta(meta);
        }
    }


}
