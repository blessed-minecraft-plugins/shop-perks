package fr.blessedraccoon.minecraft.plugins.shopperks.games.ballsofsteel;

import fr.blessedraccoon.minecraft.plugins.shopperks.shop.GenericMaterial;
import fr.blessedraccoon.minecraft.plugins.shopperks.shop.ItemPerk;
import fr.blessedraccoon.minecraft.plugins.shopperks.shop.LevelCostDescription;
import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.List;

public class PickaxeBetter extends ItemPerk {

    public PickaxeBetter(int ID) {
        super(ID);
    }

    @Override
    public ItemStack buildItem() {
        ItemStack item = new ItemStack(Material.IRON_PICKAXE);
        var meta = item.getItemMeta();
        meta.displayName(Component.text("Une meilleure pioche"));
        item.setItemMeta(meta);
        return item;
    }

    @Override
    public List<LevelCostDescription> buildLevelCostDescriptions() {
        var perksLevels = List.of(
                new LevelCostDescription(1, 1_000, "Pioche en fer", "Donne une pioche en fer au lieu d'une pioche en pierre"),
                new LevelCostDescription(2, 4_000, "Pioche en diamant", "Donne une pioche en diamant au lieu d'une pioche en fer"),
                new LevelCostDescription(3, 15_000, "Pioche en netherite", "Donne une pioche en netherite au lieu d'une pioche en diamant")
        );
        return perksLevels;
    }

    @Override
    public void apply(PlayerInventory inventory, int level) {
        var items = GenericMaterial.PICKAXES.find(inventory);
        for(var item : items) {
            switch (level) {
                case 1:
                    item.setType(Material.IRON_PICKAXE);
                    break;
                case 2:
                    item.setType(Material.DIAMOND_PICKAXE);
                    break;
                case 3:
                    item.setType(Material.NETHERITE_PICKAXE);
                    break;
            }
        }
    }

}
